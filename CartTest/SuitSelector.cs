﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CartTest
{
    public partial class frmSuitSelector : Form
    {
        public string suitChoice { get; set; }

        public frmSuitSelector()
        {
            suitChoice = "";
            InitializeComponent();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            var checkedButton = groupBox1.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            suitChoice = checkedButton.Tag.ToString();
            this.Close();
        }


    }
}
