﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartTest
{
    public class Deck
    {
        public List<int> available_cards = new List<int>();

        //disCards are all the cards played in the game thus far, including the activeCard
        public List<int> disCards = new List<int>();

        public int activeCard { get; set; }

        //If an 8 is the activeCard, this gets and sets the suit to be played next
        public string activeSuitOnly { get; set; }

        public Deck()
        {
            for (int cardindex = 0; cardindex <= 55; cardindex++)
            {
                available_cards.Add(cardindex);
            }

        }

        public int FlipFirstCard()
        {
            Random r = new Random();

            tryagain:
            //Go to 51 to exclude Jokers
            int rndcard = r.Next(0, 51);

            //If it's in the deck, use it
            if (available_cards.Contains(rndcard))
            {
                disCards.Add(rndcard);
                available_cards.Remove(rndcard);
            }
            else
            {
                goto tryagain;
            }

            return rndcard;
        }


    }
}
