﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartTest
{
    
    class CardMatcher
    {
        private static IEnumerable<int> clubs = Enumerable.Range(0, 12);
        private static IEnumerable<int> diamonds = Enumerable.Range(13, 12);
        private static IEnumerable<int> hearts = Enumerable.Range(26, 12);
        private static IEnumerable<int> spades = Enumerable.Range(39, 12);
        private static IEnumerable<int> jokers = Enumerable.Range(52, 4);

        private static IEnumerable<int> multsof13 = new int[] { 13, 26, 39 };
        private static IEnumerable<int> crazy8s = new int[] { 7, 20, 33, 46 };

        public CardMatcher()
        {

        }

        public Tuple<bool, string> ValidateSelection(Player player, Deck deck)
        {
            bool validationResponse = false;
            string specialCard = null;
            
            int card2Validate = player.selectedcards[0];

            if (crazy8s.Contains(deck.activeCard))
            {
                //If suit matches the suit called on the 8, it's valid
                switch (deck.activeSuitOnly)
                {
                    case ("Clubs"):
                        validationResponse = clubs.Contains(card2Validate) ? true : false;
                        break;
                    case ("Diamonds"):
                        validationResponse = diamonds.Contains(card2Validate) ? true : false;
                        break;
                    case ("Hearts"):
                        validationResponse = hearts.Contains(card2Validate) ? true : false;
                        break;
                    case ("Spades"):
                        validationResponse = spades.Contains(card2Validate) ? true : false;
                        break;
                    default:
                        goto MainLogic;
                }
                deck.activeSuitOnly = null;
                return Tuple.Create(validationResponse, specialCard);
            }


            MainLogic:
            //Main card matching logic
            
            //If suit matches, it's valid
            if (
                (clubs.Contains(card2Validate) && clubs.Contains(deck.activeCard)) ||
                (diamonds.Contains(card2Validate) && diamonds.Contains(deck.activeCard)) ||
                (hearts.Contains(card2Validate) && hearts.Contains(deck.activeCard)) ||
                (spades.Contains(card2Validate) && spades.Contains(deck.activeCard)) 
                )
            {
                validationResponse = true;
            }

            //If number matches, it's valid
            foreach (int number in multsof13)
            {
                if (((card2Validate + number) == deck.activeCard) || ((card2Validate - number) == deck.activeCard))
                {
                    validationResponse = true;
                }
            }

            //If it's an 8, it's valid
            if (crazy8s.Contains(card2Validate))
            {
                validationResponse = true;

                //If the last card played in a run is an 8
                if (crazy8s.Contains(player.selectedcards[(player.selectedcards.Count - 1)]))
                {
                    specialCard = "Crazy 8";
                }

            }

            //If it's a Joker, it's valid
            if (jokers.Contains(card2Validate))
            {
                validationResponse = true;
                specialCard = "Joker";
            }

            //Can't play duplicates yet until we code for 2 decks
            if (player.selectedcards.Count == 2)
            {
                validationResponse = false;
                //The selected cards must be the same
                //And, the 0 index must match the activeCard
            }

            if (player.selectedcards.Count >= 3)
            {
                //The selected cards must be in consecutive order
                //And, the 0 index must match the activeCard
            }

            return Tuple.Create(validationResponse, specialCard);

        }



    }
}
