﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CartTest
{
    public class Player
    {
        public List<int> hand = new List<int>();
        public List<int> selectedcards = new List<int>();

        CardMatcher cm = new CardMatcher();

        public Tuple<List<int>, List<int>> DealCards(int numberOfCards, List<int> deck)
        {
            //Create random number object
            Random r = new Random();

            //Get random numbers between 0 - 55 into hand until the parameter is met
            int counter = 0;
            while (counter < numberOfCards)
            {
                int rndcard = r.Next(0, 55);

                //If it IS in the deck...
                if (deck.Contains(rndcard))
                {
                    //...remove the card from the deck
                    deck.Remove(rndcard);

                    //and add the card to hand master list
                    hand.Add(rndcard);

                    counter++;
                }
                //If it's NOT in the deck...
                else
                {
                    //...don't increment the counter
                    continue;
                }
            }

            //Sort cards in ascending order, grouped by suit
            //hand.Sort();

            //return updated hand and deck
            return Tuple.Create(hand, deck);
        }

        public void PlayCards(Deck deck)
        {
            //The ValidateSelection method will bring back whether the card being played is valid...
            //...and whether it is a special card
            bool validPlay = cm.ValidateSelection(this, deck).Item1;
            string specialCard = cm.ValidateSelection(this, deck).Item2;
            
            //If the user changes the suit by playing a crazy 8, we will return it from this function
            //string changedsuit = null;
       
            if (validPlay)
            {
                foreach (int card in selectedcards)
                {
                    deck.disCards.Add(card);
                    hand.Remove(card);
                }

                deck.activeCard = selectedcards[(selectedcards.Count - 1)];
            }
            else
            {
                //
            }

            switch (specialCard)
            {
                case ("Crazy 8"):
                    deck.activeSuitOnly = ChooseSuit();
                    break;
                case ("Joker"):
                    PlayJoker();
                    break;
                case (null):
                    break;
                default:
                    break;
            }


        }


        public string ChooseSuit()
        {
            frmSuitSelector popup = new frmSuitSelector();
            popup.ShowDialog();
            string changedsuit = popup.suitChoice;

            return changedsuit;

        }

        public Tuple<int, List<int>> PlayJoker()
        {
            //        else if (validationResponse == "Eight")
            //{
            //    newActiveCard = RenewActiveCard(deck);

            //    frmSuitSelector popup = new frmSuitSelector(this);
            //    popup.ShowDialog();
            //}
            //else if (validationResponse == "Joker")
            //{

            //}
            return Tuple.Create(8, hand);
        }


    }
}
