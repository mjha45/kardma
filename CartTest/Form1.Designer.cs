﻿namespace CartTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnDrawCard = new System.Windows.Forms.Button();
            this.btnPlayIt = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pnlMyHand = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblAction = new System.Windows.Forms.Label();
            this.pnlPlayer1 = new System.Windows.Forms.Panel();
            this.pnlPlayer2 = new System.Windows.Forms.Panel();
            this.pnlPlayer3 = new System.Windows.Forms.Panel();
            this.pnlDealer = new System.Windows.Forms.Panel();
            this.txtActionLog = new System.Windows.Forms.RichTextBox();
            this.btnKeepIt = new System.Windows.Forms.Button();
            this.btnDeal = new System.Windows.Forms.Button();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.btnPlay = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDrawCard
            // 
            this.btnDrawCard.Enabled = false;
            this.btnDrawCard.Location = new System.Drawing.Point(10, 595);
            this.btnDrawCard.Name = "btnDrawCard";
            this.btnDrawCard.Size = new System.Drawing.Size(75, 23);
            this.btnDrawCard.TabIndex = 0;
            this.btnDrawCard.Text = "Draw Card";
            this.btnDrawCard.UseVisualStyleBackColor = true;
            this.btnDrawCard.Click += new System.EventHandler(this.btnDrawCard_Click);
            // 
            // btnPlayIt
            // 
            this.btnPlayIt.Enabled = false;
            this.btnPlayIt.Location = new System.Drawing.Point(10, 649);
            this.btnPlayIt.Name = "btnPlayIt";
            this.btnPlayIt.Size = new System.Drawing.Size(75, 23);
            this.btnPlayIt.TabIndex = 1;
            this.btnPlayIt.Text = "Play It";
            this.btnPlayIt.UseVisualStyleBackColor = true;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "c1.gif");
            this.imageList1.Images.SetKeyName(1, "c2.gif");
            this.imageList1.Images.SetKeyName(2, "c3.gif");
            this.imageList1.Images.SetKeyName(3, "c4.gif");
            this.imageList1.Images.SetKeyName(4, "c5.gif");
            this.imageList1.Images.SetKeyName(5, "c6.gif");
            this.imageList1.Images.SetKeyName(6, "c7.gif");
            this.imageList1.Images.SetKeyName(7, "c8.gif");
            this.imageList1.Images.SetKeyName(8, "c9.gif");
            this.imageList1.Images.SetKeyName(9, "c10.gif");
            this.imageList1.Images.SetKeyName(10, "cj.gif");
            this.imageList1.Images.SetKeyName(11, "cq.gif");
            this.imageList1.Images.SetKeyName(12, "ck.gif");
            this.imageList1.Images.SetKeyName(13, "d1.gif");
            this.imageList1.Images.SetKeyName(14, "d2.gif");
            this.imageList1.Images.SetKeyName(15, "d3.gif");
            this.imageList1.Images.SetKeyName(16, "d4.gif");
            this.imageList1.Images.SetKeyName(17, "d5.gif");
            this.imageList1.Images.SetKeyName(18, "d6.gif");
            this.imageList1.Images.SetKeyName(19, "d7.gif");
            this.imageList1.Images.SetKeyName(20, "d8.gif");
            this.imageList1.Images.SetKeyName(21, "d9.gif");
            this.imageList1.Images.SetKeyName(22, "d10.gif");
            this.imageList1.Images.SetKeyName(23, "dj.gif");
            this.imageList1.Images.SetKeyName(24, "dq.gif");
            this.imageList1.Images.SetKeyName(25, "dk.gif");
            this.imageList1.Images.SetKeyName(26, "h1.gif");
            this.imageList1.Images.SetKeyName(27, "h2.gif");
            this.imageList1.Images.SetKeyName(28, "h3.gif");
            this.imageList1.Images.SetKeyName(29, "h4.gif");
            this.imageList1.Images.SetKeyName(30, "h5.gif");
            this.imageList1.Images.SetKeyName(31, "h6.gif");
            this.imageList1.Images.SetKeyName(32, "h7.gif");
            this.imageList1.Images.SetKeyName(33, "h8.gif");
            this.imageList1.Images.SetKeyName(34, "h9.gif");
            this.imageList1.Images.SetKeyName(35, "h10.gif");
            this.imageList1.Images.SetKeyName(36, "hj.gif");
            this.imageList1.Images.SetKeyName(37, "hq.gif");
            this.imageList1.Images.SetKeyName(38, "hk.gif");
            this.imageList1.Images.SetKeyName(39, "s1.gif");
            this.imageList1.Images.SetKeyName(40, "s2.gif");
            this.imageList1.Images.SetKeyName(41, "s3.gif");
            this.imageList1.Images.SetKeyName(42, "s4.gif");
            this.imageList1.Images.SetKeyName(43, "s5.gif");
            this.imageList1.Images.SetKeyName(44, "s6.gif");
            this.imageList1.Images.SetKeyName(45, "s7.gif");
            this.imageList1.Images.SetKeyName(46, "s8.gif");
            this.imageList1.Images.SetKeyName(47, "s9.gif");
            this.imageList1.Images.SetKeyName(48, "s10.gif");
            this.imageList1.Images.SetKeyName(49, "sj.gif");
            this.imageList1.Images.SetKeyName(50, "sq.gif");
            this.imageList1.Images.SetKeyName(51, "sk.gif");
            this.imageList1.Images.SetKeyName(52, "jb.gif");
            this.imageList1.Images.SetKeyName(53, "jr.gif");
            this.imageList1.Images.SetKeyName(54, "jb2.gif");
            this.imageList1.Images.SetKeyName(55, "jr2.gif");
            // 
            // pnlMyHand
            // 
            this.pnlMyHand.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMyHand.Location = new System.Drawing.Point(94, 552);
            this.pnlMyHand.Name = "pnlMyHand";
            this.pnlMyHand.Size = new System.Drawing.Size(825, 221);
            this.pnlMyHand.TabIndex = 2;
            this.pnlMyHand.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlMyHand_Paint);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(9, 727);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblAction
            // 
            this.lblAction.AutoSize = true;
            this.lblAction.Location = new System.Drawing.Point(101, 536);
            this.lblAction.Name = "lblAction";
            this.lblAction.Size = new System.Drawing.Size(0, 13);
            this.lblAction.TabIndex = 4;
            // 
            // pnlPlayer1
            // 
            this.pnlPlayer1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlPlayer1.BackgroundImage")));
            this.pnlPlayer1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlPlayer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPlayer1.Location = new System.Drawing.Point(294, 12);
            this.pnlPlayer1.Name = "pnlPlayer1";
            this.pnlPlayer1.Size = new System.Drawing.Size(334, 252);
            this.pnlPlayer1.TabIndex = 5;
            // 
            // pnlPlayer2
            // 
            this.pnlPlayer2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlPlayer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPlayer2.Location = new System.Drawing.Point(10, 270);
            this.pnlPlayer2.Name = "pnlPlayer2";
            this.pnlPlayer2.Size = new System.Drawing.Size(334, 252);
            this.pnlPlayer2.TabIndex = 0;
            // 
            // pnlPlayer3
            // 
            this.pnlPlayer3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlPlayer3.BackgroundImage")));
            this.pnlPlayer3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlPlayer3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPlayer3.Location = new System.Drawing.Point(582, 270);
            this.pnlPlayer3.Name = "pnlPlayer3";
            this.pnlPlayer3.Size = new System.Drawing.Size(334, 252);
            this.pnlPlayer3.TabIndex = 0;
            // 
            // pnlDealer
            // 
            this.pnlDealer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlDealer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlDealer.Location = new System.Drawing.Point(350, 270);
            this.pnlDealer.Name = "pnlDealer";
            this.pnlDealer.Size = new System.Drawing.Size(226, 252);
            this.pnlDealer.TabIndex = 6;
            this.pnlDealer.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlDealer_Paint);
            // 
            // txtActionLog
            // 
            this.txtActionLog.Location = new System.Drawing.Point(9, 12);
            this.txtActionLog.Name = "txtActionLog";
            this.txtActionLog.Size = new System.Drawing.Size(279, 252);
            this.txtActionLog.TabIndex = 7;
            this.txtActionLog.Text = "";
            // 
            // btnKeepIt
            // 
            this.btnKeepIt.Enabled = false;
            this.btnKeepIt.Location = new System.Drawing.Point(9, 679);
            this.btnKeepIt.Name = "btnKeepIt";
            this.btnKeepIt.Size = new System.Drawing.Size(75, 23);
            this.btnKeepIt.TabIndex = 8;
            this.btnKeepIt.Text = "Keep It";
            this.btnKeepIt.UseVisualStyleBackColor = true;
            // 
            // btnDeal
            // 
            this.btnDeal.Location = new System.Drawing.Point(9, 552);
            this.btnDeal.Name = "btnDeal";
            this.btnDeal.Size = new System.Drawing.Size(75, 23);
            this.btnDeal.TabIndex = 9;
            this.btnDeal.Text = "Deal";
            this.btnDeal.UseVisualStyleBackColor = true;
            this.btnDeal.Click += new System.EventHandler(this.btnDeal_Click);
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "b1fv.gif");
            // 
            // btnPlay
            // 
            this.btnPlay.Enabled = false;
            this.btnPlay.Location = new System.Drawing.Point(9, 552);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(79, 23);
            this.btnPlay.TabIndex = 10;
            this.btnPlay.Text = "Play Selected";
            this.btnPlay.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 785);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.btnDeal);
            this.Controls.Add(this.btnKeepIt);
            this.Controls.Add(this.txtActionLog);
            this.Controls.Add(this.pnlDealer);
            this.Controls.Add(this.pnlPlayer3);
            this.Controls.Add(this.pnlPlayer2);
            this.Controls.Add(this.pnlPlayer1);
            this.Controls.Add(this.lblAction);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.pnlMyHand);
            this.Controls.Add(this.btnPlayIt);
            this.Controls.Add(this.btnDrawCard);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDrawCard;
        private System.Windows.Forms.Button btnPlayIt;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel pnlMyHand;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lblAction;
        private System.Windows.Forms.Panel pnlPlayer1;
        private System.Windows.Forms.Panel pnlPlayer2;
        private System.Windows.Forms.Panel pnlPlayer3;
        private System.Windows.Forms.Panel pnlDealer;
        private System.Windows.Forms.RichTextBox txtActionLog;
        private System.Windows.Forms.Button btnKeepIt;
        private System.Windows.Forms.Button btnDeal;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Button btnPlay;
    }
}

