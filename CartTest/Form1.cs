﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CartTest
{
    public partial class Form1 : Form
    {
        int x;
        int y;

        Player me = new Player();
        Player player2 = new Player();
        Player dealer = new Player();

        Deck deck = new Deck();
    
        public Form1()
        {
            InitializeComponent();
        }

        private void btnDrawCard_Click(object sender, EventArgs e)
        {
            Deal(me, 1);
            DisplayCards(me, pnlMyHand, true);

            txtActionLog.Text += " " + deck.available_cards.Count.ToString();
 
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            btnPlay.Hide();
            txtActionLog.Text += deck.available_cards.Count.ToString();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }

        private void pnlMyHand_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void pnlDealer_Paint(object sender, PaintEventArgs e)
        {
            pnlDealer.BackColor = Color.LightGreen;
        }

        private void pbi_DoubleClick(object sender, EventArgs e)
        {
            PictureBox pb = sender as PictureBox;
            if (pb.BorderStyle == BorderStyle.Fixed3D)
            {
                pb.BorderStyle = BorderStyle.None;
                me.selectedcards.Remove(Convert.ToInt32(pb.Tag));
                lblAction.Text = "You unselected " + pb.Tag.ToString();
            }
            else
            {
                pb.BorderStyle = BorderStyle.Fixed3D;
                me.selectedcards.Add(Convert.ToInt32(pb.Tag));
                lblAction.Text = "You clicked " + pb.Tag.ToString();
            }

            btnPlay.Enabled = me.selectedcards.Count > 0 ? true : false;
        }

        private void btnDeal_Click(object sender, EventArgs e)
        {
            btnDeal.Dispose();

            Deal(me, 8);
            Deal(player2, 8);

            DisplayCards(me, pnlMyHand, true);
            DisplayCards(player2, pnlPlayer2, false);

            //Keep drawing for first card to play until it's not a Joker

            
            deck.activeCard = deck.FlipFirstCard();
            dealer.hand.Add(deck.activeCard);
            DisplayCards(dealer, pnlDealer, true);
            


            btnDrawCard.Enabled = true;
            btnPlay.Show();

        }

        private void Deal(Player player, int numberofcards)
        {
            var returnvalue = player.DealCards(numberofcards, deck.available_cards);
            player.hand = returnvalue.Item1;
            deck.available_cards = returnvalue.Item2;
        }


        private void DisplayCards(Player player, Panel panel, bool showhand)
        {
            //Clear and dispose the existing pictureboxes (cards) from the panel
            panel.Controls.Clear();
            foreach (Control c in panel.Controls)
                c.Dispose();

            //Create array of pictureboxes to hold each card's image
            PictureBox[] pb = new PictureBox[player.hand.Count];

            //Set x and y coordinates of the cards to display back to 0
            x = 0;
            y = 0;

            //Render each picturebox to screen
            for (int cardID = 0; cardID < player.hand.Count; cardID++)
            {
                //Each element in the pb array is its own picturebox
                pb[cardID] = new PictureBox();
                pb[cardID].BorderStyle = BorderStyle.None;

                //Associate each new picturebox with hand
                pb[cardID].Tag = player.hand[cardID];

                //Render appropriate image from imageList object to picturebox image
                pb[cardID].Image = showhand ? imageList1.Images[player.hand[cardID]] : imageList2.Images[0];                     

                //Set pb's size/location, add it to the panel, and wait 200ms before looping to the next card
                pb[cardID].Size = new Size(72, 100);
                pb[cardID].Location = new Point(x, y);
                panel.Controls.Add(pb[cardID]);
                Application.DoEvents();
                //System.Threading.Thread.Sleep(75);

                //Set space between each card
                x = x + 75;

                //Wrap around the panel if image exceeds border
                if (panel.Size.Width - 50 < x)
                {
                    y = y + 100;
                    x = 10;
                }
            }

            //lblAction.Text = "Double click on the card(s) you wish to play";

            for (int pickcard = 0; pickcard < player.hand.Count; pickcard++)
            {
                pb[pickcard].DoubleClick += new EventHandler(pbi_DoubleClick);
            }

        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            //var returnvalue = me.PlayCards(deck);
            //deck.activeCard = returnvalue.Item1;

            me.PlayCards(deck);
            if (deck.activeSuitOnly == null)
            {
                lblAction.Text = "The card is now " + deck.activeCard;
            }
            else
            {
                lblAction.Text = "The active suit is now " + deck.activeSuitOnly;
            }
            
            dealer.hand.Clear();
            dealer.hand.Add(deck.activeCard);

            DisplayCards(me, pnlMyHand, true);
            DisplayCards(dealer, pnlDealer, true);


        }

        

    }
}
