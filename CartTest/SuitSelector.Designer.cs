﻿namespace CartTest
{
    partial class frmSuitSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnGo = new System.Windows.Forms.Button();
            this.radSpades = new System.Windows.Forms.RadioButton();
            this.radHearts = new System.Windows.Forms.RadioButton();
            this.radDiamonds = new System.Windows.Forms.RadioButton();
            this.radClubs = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.btnGo);
            this.groupBox1.Controls.Add(this.radSpades);
            this.groupBox1.Controls.Add(this.radHearts);
            this.groupBox1.Controls.Add(this.radDiamonds);
            this.groupBox1.Controls.Add(this.radClubs);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(183, 188);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pick a Suit";
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(48, 159);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(75, 23);
            this.btnGo.TabIndex = 4;
            this.btnGo.Text = "Go";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // radSpades
            // 
            this.radSpades.AutoSize = true;
            this.radSpades.Location = new System.Drawing.Point(48, 118);
            this.radSpades.Name = "radSpades";
            this.radSpades.Size = new System.Drawing.Size(61, 17);
            this.radSpades.TabIndex = 3;
            this.radSpades.TabStop = true;
            this.radSpades.Tag = "Spades";
            this.radSpades.Text = "Spades";
            this.radSpades.UseVisualStyleBackColor = true;
            // 
            // radHearts
            // 
            this.radHearts.AutoSize = true;
            this.radHearts.Location = new System.Drawing.Point(48, 95);
            this.radHearts.Name = "radHearts";
            this.radHearts.Size = new System.Drawing.Size(56, 17);
            this.radHearts.TabIndex = 2;
            this.radHearts.TabStop = true;
            this.radHearts.Tag = "Hearts";
            this.radHearts.Text = "Hearts";
            this.radHearts.UseVisualStyleBackColor = true;
            // 
            // radDiamonds
            // 
            this.radDiamonds.AutoSize = true;
            this.radDiamonds.Location = new System.Drawing.Point(48, 72);
            this.radDiamonds.Name = "radDiamonds";
            this.radDiamonds.Size = new System.Drawing.Size(72, 17);
            this.radDiamonds.TabIndex = 1;
            this.radDiamonds.TabStop = true;
            this.radDiamonds.Tag = "Diamonds";
            this.radDiamonds.Text = "Diamonds";
            this.radDiamonds.UseVisualStyleBackColor = true;
            // 
            // radClubs
            // 
            this.radClubs.AutoSize = true;
            this.radClubs.Location = new System.Drawing.Point(48, 49);
            this.radClubs.Name = "radClubs";
            this.radClubs.Size = new System.Drawing.Size(51, 17);
            this.radClubs.TabIndex = 0;
            this.radClubs.TabStop = true;
            this.radClubs.Tag = "Clubs";
            this.radClubs.Text = "Clubs";
            this.radClubs.UseVisualStyleBackColor = true;
            // 
            // frmSuitSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(207, 212);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmSuitSelector";
            this.Text = "Suit Selector";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radSpades;
        private System.Windows.Forms.RadioButton radHearts;
        private System.Windows.Forms.RadioButton radDiamonds;
        private System.Windows.Forms.RadioButton radClubs;
        private System.Windows.Forms.Button btnGo;
    }
}